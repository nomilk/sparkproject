package fr.iiil.bigdata.naissance.functions;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fr.iiil.bigdata.naissance.writer.GroupByWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class WriterTest {
    @Test
    public void WriteCsv(){
        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("3il.path.input");
        String outputPathStr = config.getString("3il.path.output");

        String newFile = "";

        File dir = new File(outputPathStr);
        SparkSession sparkSession = SparkSession.builder()
                .master("local[*]")
                .appName("testing")
                .getOrCreate();


        List<String> points = Arrays.asList("LO", "AU", "SA", "NK");
        Dataset<Row> actual = sparkSession.createDataset(points, Encoders.STRING()).toDF("Initials");

        GroupByWriter writer = new GroupByWriter(outputPathStr);
        writer.accept(actual);

        for (File f : dir.listFiles()) {
            if (f.isFile() && f.getName().endsWith(".csv")) {
                newFile = f.getName();
            }
        }

        Dataset<Row> expected = sparkSession
                .read()
                .option("delimiter", ";")
                .option( "header", "true")
                .csv(outputPathStr+'/'+newFile);

        assertThat(actual
                .select("Initials")
                .collectAsList()
                .stream()
                .map(f -> f.getString(0)).toArray())
                .containsExactlyInAnyOrder("LO", "AU", "SA", "NK");


    }

}
