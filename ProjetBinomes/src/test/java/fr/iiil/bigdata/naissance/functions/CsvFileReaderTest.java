package fr.iiil.bigdata.naissance.functions;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fr.iiil.bigdata.naissance.reader.CsvFileReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CsvFileReaderTest {
    Config config = ConfigFactory.load();
    String inputPathStr = config.getString("3il.path.input");
    @Test
    public void fileTest() {
        CsvFileReader reader = new CsvFileReader(inputPathStr);
        Dataset<Row> lines = reader.get();
        assertThat(lines.first().toString()).contains("Masculin");
    }
}
