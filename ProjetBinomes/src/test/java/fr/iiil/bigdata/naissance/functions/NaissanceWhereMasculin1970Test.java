package fr.iiil.bigdata.naissance.functions;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fr.iiil.bigdata.naissance.reader.CsvFileReader;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NaissanceWhereMasculin1970Test {
    @Test
    public void fileTest(){

        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("3il.path.input");
        SparkConf sparkConf= new SparkConf().setMaster("local[2]").setAppName("Hello World");
        SparkSession sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();
        CsvFileReader reader = new CsvFileReader(inputPathStr);
        Dataset<Row> lines = reader.get();

        Dataset<Row> dataset = MasculinNeeGroup.builder().build().apply(lines);

        assertThat(dataset.first().toString()).contains("1987");
    }
}
