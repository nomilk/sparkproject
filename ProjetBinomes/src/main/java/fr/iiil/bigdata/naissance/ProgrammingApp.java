package fr.iiil.bigdata.naissance;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import fr.iiil.bigdata.naissance.functions.MasculinNeeGroup;
import fr.iiil.bigdata.naissance.functions.NaissanceWhereMasculin1970;
import fr.iiil.bigdata.naissance.reader.CsvFileReader;
import fr.iiil.bigdata.naissance.writer.GroupByWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Hello world!
 *
 */
@Slf4j
public class ProgrammingApp {
    public static void main(String[] args) {
        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("3il.path.input");
        String outputPathStr = config.getString("3il.path.output");


        SparkConf sparkConf= new SparkConf().setMaster("local[2]").setAppName("Hello World");
        SparkSession sparkSession= SparkSession.builder().config(sparkConf).getOrCreate();
        CsvFileReader reader = new CsvFileReader(inputPathStr, sparkSession);
        Dataset<Row> lines = reader.get();


        Dataset<Row> ds = MasculinNeeGroup.builder().build().apply(lines);
        ds.show(false);

        Dataset<Row> dsSelect = NaissanceWhereMasculin1970.builder().build().apply(lines);
        dsSelect.show(false);

        GroupByWriter groupByWriter = new GroupByWriter(outputPathStr + "/resultGroupBy");
        groupByWriter.accept(ds);

        try {
            Thread.sleep(1000*60*10);
        }catch (InterruptedException interruptedException){
            interruptedException.printStackTrace();
        }

    }

}
