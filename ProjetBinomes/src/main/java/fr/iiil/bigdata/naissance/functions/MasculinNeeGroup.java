package fr.iiil.bigdata.naissance.functions;

import lombok.Builder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.function.Function;

import static org.apache.spark.sql.functions.count;

@Builder
public class MasculinNeeGroup implements Function<Dataset<Row>, Dataset<Row>> {

    @Override
    public Dataset<Row> apply(Dataset<Row> lines) {
        return lines.groupBy("ANNEE")
                .agg(count("NOMBRE_OCCURENCES"));
    }
}
