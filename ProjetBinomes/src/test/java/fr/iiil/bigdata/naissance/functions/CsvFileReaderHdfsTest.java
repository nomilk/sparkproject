package fr.iiil.bigdata.naissance.functions;


import fr.iiil.bigdata.naissance.reader.CsvFileReaderHdfs;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class CsvFileReaderHdfsTest {
    @Test
    public void testFileExists() throws IOException {
        String inputPathStr = "src/main/resources/output/resultGroupBy/part-00000-0d7c19fc-3378-4049-a063-77c64574d12b-c000.csv";
        Configuration hadoopConf = new Configuration();
        FileSystem hdfs = FileSystem.get(hadoopConf);

        assertThat(hdfs.exists(new Path(inputPathStr))).isEqualTo(true);
    }

    @Test
    public void testFileReader() throws IOException {
        String inputPathStr = "src/main/resources/output/resultGroupBy/part-0000-0d7c19fc-3378-4049-a063-77c64574d12b-c000.csv";
        CsvFileReaderHdfs csvFileReader = new CsvFileReaderHdfs(inputPathStr);
        Dataset<Row> result = csvFileReader.get();

        assertThat(result.isEmpty()).isEqualTo(true);

    }
}
