package fr.iiil.bigdata.naissance.reader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;



@Slf4j
@RequiredArgsConstructor
public class CsvFileReader implements Supplier<Dataset<Row>> {
    private final String inputPathStr;
    private final SparkSession sparkSession;
    //    public TextFileReader(String inputPathStr) {
//        this.inputPathStr = inputPathStr;
//        this.sparkSession = SparkSession.builder().config(new SparkConf().setMaster("local[2]").setAppName("AppSpark")).getOrCreate();
//
//    }
    public CsvFileReader(String inputPathStr) {
        this(inputPathStr, SparkSession.builder().config(new SparkConf().setMaster("local[2]").setAppName("AppSpark")).getOrCreate());
    }
    @Override
    public Dataset<Row> get() {
        log.info("reading file at inputPathStr={}", inputPathStr);
        try {
            Dataset<Row> lines = sparkSession.read().option("delimiter", ",").option("header", "true").csv(inputPathStr);
            return lines;
        } catch (Exception exception) {
            log.error("failed to read file at inputPathStr={} due to ...", inputPathStr, exception);
        }
        return null;

    }
}
