package fr.iiil.bigdata.naissance.functions;

import lombok.Builder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.function.Function;

import static org.apache.spark.sql.functions.col;

@Builder
public class NaissanceWhereMasculin1970 implements Function<Dataset<Row>, Dataset<Row>> {
    //private final String Sexe;
    @Override
    public Dataset<Row> apply(Dataset<Row> lines) {
        Dataset<Row> dt = lines.filter(col("ENFANT_SEXE").contains("Masculin"));
        return dt.filter(col("Annee").contains("1970"));
    }

}
