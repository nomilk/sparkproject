package fr.iiil.bigdata.naissance.reader;

import lombok.SneakyThrows;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

public class CsvFileReaderHdfs implements Supplier<Dataset<Row>> {
    private final String inputPathStr;
    private final SparkSession sparkSession;
    private final Configuration hadoopConf;
    private FileSystem hdfs;

    public CsvFileReaderHdfs(String inputPathStr) {
        this.inputPathStr = inputPathStr;
        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("Hello World");
        this.sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();
        this.hadoopConf = new Configuration();
    }

    @SneakyThrows
    @Override
    public Dataset<Row> get() {
        Dataset<Row> rows = sparkSession.emptyDataFrame();
        this.hdfs = FileSystem.get(hadoopConf);
        if(this.hdfs.exists(new Path(inputPathStr))) {
            rows = sparkSession
                    .read()
                    .option("delimiter",",")
                    .option("header","true")
                    .csv(inputPathStr);
        }
        return rows;
    }
}
